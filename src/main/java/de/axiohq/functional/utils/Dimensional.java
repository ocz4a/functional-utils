/*
 * Copyright 2020 Axio Components for Business.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.axiohq.functional.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Stream;

public class Dimensional {
	/**
	 * Creates a new List with an item appended
	 * @param list a List of items 
	 * @param item another item
	 * @return A new copy of the list with the item appended
	 */
	public final static <T> List<T> listAdd(List<T> list, T item) {
		List<T> r = new ArrayList<>(list);
		r.add(item);
		return r;
	}
	
	private static Function<List<List<?>>, Stream<List<?>>> cartesian(List<Object> c) {
		return v -> v.isEmpty()	
				? Stream.of(c) 
				: v.get(0).stream().flatMap(p -> cartesian(listAdd(c, p)).apply(v.subList(1, v.size())));
	}

	/**
	 * Computes the cartesian product of a collection of Lists
	 * @param v an array of Lists
	 * @return The cartesian product the Lists 
	 */
	public final static Stream<List<?>> cartesian(List<?> ...v) {
		List<List<?>> args = Arrays.asList(v);
		return cartesian(new ArrayList<>(v.length)).apply(args);
	}

}
